#OCP grooming

This project contains code snippets that helps to prepare taking the Java SE 8 Programmer II exam. 
The main purpose is to store the fully functional and launch-ready code to save the practitioner's time.

In the package:
```org.ts.ocp.udemy```
you can find the code snippets of the Udemy's course: Java Certification - OCP (1Z0-809) Topic-wise Tests [2019] by Udayan Khattry

Feel free to create pull requests and add new code snippets, but I have one request... 

Please stick to the following convention:  
1. Each test should have its own separate package i.e.: Test 6 should be located in ```org.ts.ocp.udemy.test6``` package  
2. Each question snippet should be represented by the class of name: ```Question<question_number>``` i.e.: Question3  
3. Each questions class (all together with supplementary classes) should be located in a separate package of name ```q<question_number>``` i.e.: question 4 of test 6 should be located in ```org.ts.ocp.udemy.test6.q6```  
