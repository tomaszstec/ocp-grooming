package org.ts.ocp.test3.q108;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

@SuppressWarnings({"Convert2MethodRef", "FunctionalExpressionCanBeFolded"})
public class Question108 {
    public static void main(String[] args) {
        /* INSERT */
        
        Function<Character,Character> operator = x->(char)(x+1);     // answer E
        List<Character> vowels = Arrays.asList('A', 'E', 'I', 'O', 'U');
        vowels.stream().map(x -> operator.apply(x)).forEach(System.out::print); //Line n1
    }
}
