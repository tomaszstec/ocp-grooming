package org.ts.ocp.test3.q104;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

@SuppressWarnings({"ConstantConditions", "InvalidComparatorMethodReference"})
public class Question104 {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(10, 20, 8);
        
        System.out.println(list.stream().max(Comparator.comparing(a -> a)).get()); //Line 1
        
        System.out.println(list.stream().max(Integer::compareTo).get()); //Line 2
        
        System.out.println(list.stream().max(Integer::max).get()); //Line 3
    }
}
