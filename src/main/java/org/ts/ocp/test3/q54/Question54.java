package org.ts.ocp.test3.q54;

import java.util.Optional;

@SuppressWarnings("ConstantConditions")
public class Question54 {
    public static void main(String[] args) {
        Optional<Integer> optional = Optional.of(null); //Line 8
        System.out.println(optional.orElse(-1)); //Line 9
    }
}
