package org.ts.ocp.test3.q61;

import java.util.OptionalInt;

@SuppressWarnings("unused")
class MyException extends Exception {}

@SuppressWarnings("unused")
public class Question61 {
    public static void main(String[] args) {
        OptionalInt optional = OptionalInt.empty();
//        System.out.println(optional.orElseThrow(MyException::new));   //compilation error because of not caught checked exception
    }
}
