package org.ts.ocp.test3.q73;

import java.util.Arrays;
import java.util.stream.Stream;

@SuppressWarnings("SimplifyStreamApiCallChains")
public class Question73 {
    public static void main(String[] args) {
        Stream<String> stream = Arrays.asList("One", "Two", "Three")
                                      .stream();
        System.out.println(stream.reduce(null, (s1, s2) -> s1 + s2));
    }
}
