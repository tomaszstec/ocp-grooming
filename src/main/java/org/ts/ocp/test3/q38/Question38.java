package org.ts.ocp.test3.q38;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

@SuppressWarnings({"unused", "UnnecessaryInterfaceModifier"})
interface StringConsumer extends Consumer<String> {
    @Override
    public default void accept(String s) {
        System.out.println(s.toUpperCase());
    }
}

@SuppressWarnings("unused")
public class Question38 {
    public static void main(String[] args) {
//        StringConsumer consumer = s -> System.out.println(s.toLowerCase());    //compilation error!!!
        List<String>   list     = Arrays.asList("Dr", "Mr", "Miss", "Mrs");
//        list.forEach(consumer);
    }
}
