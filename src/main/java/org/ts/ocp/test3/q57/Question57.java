package org.ts.ocp.test3.q57;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("WeakerAccess")
class Rope {
    int    length;
    String color;
    
    Rope(int length, String color) {
        this.length = length;
        this.color = color;
    }
    
    public String toString() {
        return "Rope [" + color + ", " + length + "]";
    }
    
    static class RedRopeFilter {
        boolean filter(Rope rope) {
            return rope.color.equalsIgnoreCase("Red");
        }
    }
}

public class Question57 {
    public static void main(String[] args) {
        List<Rope> list = new ArrayList<>();
        list.add(new Rope(5, "red"));
        list.add(new Rope(10, "Red"));
        list.add(new Rope(7, "RED"));
        list.add(new Rope(10, "green"));
        list.add(new Rope(7, "Blue"));
        
        list.stream()
            .filter(new Rope.RedRopeFilter()::filter)
            .forEach(System.out::println); //Line n1
    }
}
