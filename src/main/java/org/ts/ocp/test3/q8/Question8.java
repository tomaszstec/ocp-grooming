package org.ts.ocp.test3.q8;

import java.util.function.Function;

public class Question8 {
    public static void main(String[] args) {
        Function<Integer, Integer> f = x -> x + 10;
        Function<Integer, Integer> g = y -> y * y;
        
        Function<Integer, Integer> fog = g.compose(f); //Line 8
        System.out.println(fog.apply(10));
        answerA(f,g);
        answerB(f,g);
        answerC(f,g);
    }
    
    private static void answerA(Function<Integer, Integer> f, Function<Integer, Integer> g) {
        System.out.format("%s: %d%n", new Throwable()
                                              .getStackTrace()[0]
                                              .getMethodName(), f.compose(g).apply(10));
    }
    
    private static void answerB(Function<Integer, Integer> f, Function<Integer, Integer> g) {
        System.out.format("%s: %d%n", new Throwable()
                                              .getStackTrace()[0]
                                              .getMethodName(),f.andThen(g).apply(10));
    }
    
    private static void answerC(Function<Integer, Integer> f, Function<Integer, Integer> g) {
        System.out.format("%s: %d%n", new Throwable()
                                              .getStackTrace()[0]
                                              .getMethodName(),g.andThen(f).apply(10));
    }
}
