package org.ts.ocp.test3.q72;

import java.util.stream.IntStream;

@SuppressWarnings("UnusedAssignment")
public class Question72 {
    public static void main(String[] args) {
        int       res    = 1;
        IntStream stream = IntStream.rangeClosed(1, 4);
        
        System.out.println(stream.reduce(res++, (i, j) -> i * j));
    }
}
