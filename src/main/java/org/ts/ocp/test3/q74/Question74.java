package org.ts.ocp.test3.q74;

import java.util.Arrays;
import java.util.function.Supplier;
import java.util.stream.Stream;

@SuppressWarnings({"SimplifyStreamApiCallChains", "unused"})
public class Question74 {
    public static void main(String[] args) {
        Stream<Double> stream = Arrays.asList(1.8, 2.2, 3.5)
                                      .stream();
        
        /*INSERT*/
        Supplier<Stream<Double>> supplier = () -> Arrays.asList(1.8, 2.2, 3.5)
                                                        .stream();
        answerA(supplier.get());
        answerB(supplier.get());
        answerC(supplier.get());
        answerD(supplier.get());
        answerE(supplier.get());
    }
    
    private static void answerA(Stream<Double> stream) {
        System.out.format("%s: %.1f%n",
                          new Throwable().getStackTrace()[0].getMethodName(),
                          stream.reduce(0.0, (d1, d2) -> d1 + d2));
    }
    
    private static void answerB(Stream<Double> stream) {
        System.out.format("%s: %.1f%n",
                          new Throwable().getStackTrace()[0].getMethodName(), stream.reduce(0.0, Double::sum));
    }
    
    private static void answerC(Stream<Double> stream) {
        System.out.format("%s: %.1f%n",
                          new Throwable().getStackTrace()[0].getMethodName(), stream.reduce(0.0, (d1, d2) -> d1 + d2));
    }
    
    private static void answerD(Stream<Double> stream) {
        System.out.format("%s: %.1f%n",
                          new Throwable().getStackTrace()[0].getMethodName(), stream.reduce(0.0, Double::sum));
    }
    
    private static void answerE(Stream<Double> stream) {
//        System.out.println(stream.sum());   //compilation error: there is no sum() method on generic stream
        System.out.format("%s: Compilation error: there is no sum() method in generic stream",
                          new Throwable().getStackTrace()[0].getMethodName());
    }
}
