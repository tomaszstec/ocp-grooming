package org.ts.ocp.test3.q13;

import java.util.function.Consumer;

@SuppressWarnings("UnusedAssignment")
public class Question13 {
    public static void main(String[] args) {
        Consumer<Integer> consumer = System.out::print;
        Integer           i        = 5;
        consumer.andThen(consumer)
                .accept(
                        i++); //Line 7
    }
}
