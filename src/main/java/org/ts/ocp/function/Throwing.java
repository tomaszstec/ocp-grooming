package org.ts.ocp.function;

import java.util.function.Consumer;
import java.util.function.Function;

public final class Throwing {
    private Throwing() {
    }

    public static <T, R> Function<T, R> throwingFunction(ThrowingFunction<T, R, Exception> throwingFunction) {
        return i -> {
            try {
                return throwingFunction.apply(i);
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        };
    }

    public static <T> Consumer<T> throwingConsumer(ThrowingConsumer<T, Exception> throwingConsumer) {
        return i -> {
            try {
                throwingConsumer.accept(i);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        };
    }
}
