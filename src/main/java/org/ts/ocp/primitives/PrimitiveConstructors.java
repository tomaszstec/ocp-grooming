package org.ts.ocp.primitives;

@SuppressWarnings({"unused", "UnnecessaryBoxing", "BooleanConstructorCall"})
public class PrimitiveConstructors {
    public static void main(String[] args) {
        Boolean b = new Boolean(null);   // ok: return false
//        new Byte(null);        // NumberFormatException is thrown
//        new Short(null);       // NumberFormatException is thrown
//        new Integer(null);     // NumberFormatException is thrown
//        new Character(null);   //compilation error: Character class has only one constructor: Character(char c)
//        new Float(null);       // NullPointerException is thrown
//        new Double(null);      // NullPointerException is thrown
    }
}
