package org.ts.ocp.test6;

import org.ts.ocp.function.Throwing;

import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

import static org.ts.ocp.function.Throwing.throwingConsumer;
import static org.ts.ocp.function.Throwing.throwingFunction;

public class DerbySetup {
    private DerbySetup() {
    }
    
    private static final String DEFAULT_DATABASE_NAME = "ocp";
    
    @SuppressWarnings("WeakerAccess")
    public static String setupDerby(final String databaseName,
                                    List<String> ddlSqls,
                                    List<String> dmlSqls, boolean debug) {
        final String url = String.format("jdbc:derby:%s;create=true",
                                         Objects.toString(databaseName, DEFAULT_DATABASE_NAME));
        final Locale previousLocale = Locale.getDefault();
        try (Connection connection = DriverManager.getConnection(url); Statement statement = connection
                                                                                                     .createStatement
                                                                                                              ()) {
            Locale.setDefault(Locale.US);
            printDatabaseMetaDataInformation(connection, debug);
            
            deleteAllNonSystemTables(connection, debug);
            
            executeMultipleUpdates(ddlSqls, statement, debug);
            executeMultipleUpdates(dmlSqls, statement, debug);
            return url;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            Locale.setDefault(previousLocale);
        }
    }
    
    public static String setupDerby(List<String> ddlSqls, List<String> dmlSqls) {
        return setupDerby(null, ddlSqls, dmlSqls, false);
    }
    
    private static void executeMultipleUpdates(List<String> sqls, Statement statement, boolean debug) {
        if (debug) {
            sqls.stream()
                .peek(System.out::println)
                .forEach(Throwing.throwingConsumer(statement::executeUpdate));
        } else {
            sqls.forEach(Throwing.throwingConsumer(statement::executeUpdate));
        }
    }
    
    private static void printDatabaseMetaDataInformation(Connection conn, boolean debug) throws SQLException {
        if (debug) {
            final DatabaseMetaData databaseMetaData = conn.getMetaData();
            printSelectedTableColumns(databaseMetaData
                                              .getTableTypes());
            printSelectedTableColumns(databaseMetaData
                                              .getSchemas());
            printSelectedTableColumns(conn.getMetaData()
                                          .getTables(null, null, "%", new String[]{"TABLE"}), 3, 4);
        }
    }
    
    public static void printSelectedTableColumns(ResultSet resultSet,
                                                 int... columnIndexes) throws SQLException {
        final ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
        final String tableNames = Arrays.stream(columnIndexes)
                                        .boxed()
                                        .map(Throwing.throwingFunction(resultSetMetaData::getTableName))
                                        .distinct()
                                        .collect(Collectors.joining(","));
        System.out.format("Table: %s%n", tableNames);
        toStringList(resultSet, columnIndexes).forEach(System.out::println);
    }
    
    private static List<List<String>> toStringList(ResultSet rs, int... columnIndexes) throws SQLException {
        List<List<String>> result = new ArrayList<>();
        while (rs.next()) {
            List<String> columnValues = new ArrayList<>();
            for (int columnIndex : columnIndexes) {
                columnValues.add(rs.getString(columnIndex));
            }
            result.add(columnValues);
        }
        return result;
    }
    
    private static void deleteAllNonSystemTables(Connection connection, boolean debug) throws SQLException {
        getAllNonSystemTaleNames(connection).stream()
                                            .map("DROP TABLE "::concat)
                                            .peek(sql -> {
                                                if (debug) {
                                                    System.out.println(sql);
                                                }
                                            })
                                            .map(throwingFunction(connection::prepareStatement))
                                            .forEach(throwingConsumer(PreparedStatement::executeUpdate));
        
    }
    
    private static List<String> getAllNonSystemTaleNames(Connection connection) throws SQLException {
        final ResultSet rs = connection.getMetaData()
                                       .getTables(null, null, "%", new String[]{"TABLE"});
        return toStringList(rs, 3).stream()
                                  .flatMap(Collection::stream)
                                  .collect(Collectors.toList());
    }
    
}