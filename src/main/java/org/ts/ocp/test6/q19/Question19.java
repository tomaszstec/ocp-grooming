package org.ts.ocp.test6.q19;

import java.util.stream.IntStream;

public class Question19 {
    public static void main(String[] args) {
        //noinspection OptionalGetWithoutIsPresent
        int res = IntStream.rangeClosed(1, 1000).parallel()
                           .filter( i -> i > 50).findFirst().getAsInt();
        System.out.println(res);
    }
}
