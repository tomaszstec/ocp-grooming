package org.ts.ocp.test6.q10;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class Question10 {
    public static void main(String[] args) {
        List<String> list = new CopyOnWriteArrayList<>();
        list.add("Melon");
        list.add("Apple");
        list.add("Banana");
        list.add("Mango");
        for(String s : list) {
            list.removeIf(str -> str.startsWith("M"));
            System.out.println(s);
        }
    }
}
