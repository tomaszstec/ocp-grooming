package org.ts.ocp.test6.q61;

import org.ts.ocp.test6.DerbySetup;

import java.sql.*;
import java.util.Collections;

@SuppressWarnings({"unused", "RedundantThrows", "UnusedReturnValue", "ResultOfMethodCallIgnored"})
public class Question61 {
    public static void main(String[] args) {
        final String url = DerbySetup.setupDerby(Collections.emptyList(),Collections.emptyList());
        try(Connection connection =DriverManager.getConnection(url)){
            answerA(connection);
            answerB(connection);
            answerC(connection);
            answerD(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    private static Statement answerA(Connection connection) throws SQLException {
//        return connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE);   //compilation error: cannot resolve method...
        return null;
    }
    private static Statement answerB(Connection connection) throws SQLException {
//        return connection.createStatement(ResultSet.CONCUR_READ_ONLY);   //compilation error: cannot resolve method...
        return null;
    }
    private static Statement answerC(Connection connection) throws SQLException {
        return connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
    }
    private static Statement answerD(Connection connection) throws SQLException {
        return connection.createStatement();
    }
}
