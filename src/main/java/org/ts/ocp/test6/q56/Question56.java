package org.ts.ocp.test6.q56;

import org.ts.ocp.test6.DerbySetup;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

@SuppressWarnings({"SqlNoDataSourceInspection", "SqlResolve"})
public class Question56 {
    public static void main(String[] args) throws Exception {
        String url = DerbySetup.setupDerby(getDdlSqls(), getDmlSqls());
        Properties prop = new Properties();
//        user and password is not used because we don't use mysql but derby
//        prop.put("user", "root");
//        prop.put("password", "password");
        String query = "Select ID, FIRSTNAME, LASTNAME, SALARY FROM EMPLOYEE ORDER BY ID";
        Class.forName(url);

        try (Connection con = DriverManager.getConnection(url, prop);
             Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
             ResultSet rs = stmt.executeQuery(query)) {
            rs.relative(1);
            System.out.println(rs.getString(2));
        }
    }

    private static List<String> getDdlSqls() {
        return Collections.singletonList(
                "CREATE TABLE EMPLOYEE (ID INTEGER, FIRSTNAME VARCHAR(100), LASTNAME VARCHAR(100), SALARY REAL, PRIMARY KEY (ID))");
    }

    private static List<String> getDmlSqls() {
        return Arrays.asList("INSERT INTO EMPLOYEE VALUES(101,'John','Smith',12000)",
                             "INSERT INTO EMPLOYEE VALUES(102,'Sean','Smith', 15000)",
                             "INSERT INTO EMPLOYEE VALUES(103,'Regina','Williams',15500)",
                             "INSERT INTO EMPLOYEE VALUES(104,'Natasha','George',14600)");
    }
}
