package org.ts.ocp.test6.q15;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class Player extends Thread {
    private CyclicBarrier cb;
 
    Player(CyclicBarrier cb) {
        this.cb = cb;
    }
 
    public void run() {
        try {
            cb.await();
        } catch (InterruptedException | BrokenBarrierException ignored) {}
    }
}
 
class Match implements Runnable {
    public void run() {
        System.out.println("Match is starting...");
    }
}
 
public class Question15
{
    public static void main(String[] args) {
        Match match = new Match();
        CyclicBarrier cb = new CyclicBarrier(2, match);
        ExecutorService es = Executors.newFixedThreadPool(1);
        es.execute(new Player(cb));
        es.execute(new Player(cb));
        es.shutdown();
    }
}