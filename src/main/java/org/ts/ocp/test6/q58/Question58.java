package org.ts.ocp.test6.q58;

import org.ts.ocp.test6.DerbySetup;

import java.sql.*;
import java.util.Collections;
import java.util.List;

public class Question58 {
    
    public static void main(String[] args) throws SQLException {
        String url      = DerbySetup.setupDerby(getDdlSqls(), getDmlSqls());
//        user and password is not used because we don't use mysql but derby
//        String user     = "root";
//        String password = "password";
        String query    = "Select msg1 as msg, msg2 as msg FROM MESSAGES";
        try (Connection con = DriverManager.getConnection(url/*, user, password*/);
             Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
             ResultSet rs = stmt.executeQuery(query)) {
            rs.absolute(1);
            System.out.println(rs.getString("msg"));
            System.out.println(rs.getString("msg"));
        }
    }
    
    private static List<String> getDdlSqls(){
        return Collections.singletonList("CREATE TABLE MESSAGES(msg1 VARCHAR(100), msg2 VARCHAR(100))");
    }
    
    private static List<String> getDmlSqls(){
        return Collections.singletonList("INSERT INTO MESSAGES VALUES('Happy New Year!', 'Happy Holidays!')");
    }
}
