package org.ts.ocp.test6.q23;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Question23 {
    private static void print() {
        System.out.println("PRINT");
    }

    private static Integer get() {
        return 10;
    }

    public static void main(String [] args) throws InterruptedException, ExecutionException {
        ExecutorService es = Executors.newFixedThreadPool(10);
        Future<?> future1 = es.submit(Question23::print);
        Future<?> future2 = es.submit(Question23::get);
        System.out.println(future1.get());
        System.out.println(future2.get());
        es.shutdown();
    }
}
