package org.ts.ocp.test6.q65;

import org.ts.ocp.test6.DerbySetup;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.List;

public class Question65 {
    public static void main(String[] args) throws SQLException {
        String url = DerbySetup.setupDerby(getDdlSqls(), getDmlSqls());
//        user and password is not used because we don't use mysql but derby
//        String user = "root";
//        String password = "password";
        String query = "DELETE FROM MESSAGES";
        try (Connection con = DriverManager.getConnection(url/*, user, password*/);
             Statement stmt = con.createStatement()) {
            System.out.println(stmt.execute(query));
        }
    }
    
    private static List<String> getDdlSqls() {
        return Collections.singletonList("CREATE TABLE MESSAGES(msg1 VARCHAR(100), msg2 VARCHAR(100))");
    }
    
    private static List<String> getDmlSqls() {
        return Collections.singletonList("INSERT INTO MESSAGES VALUES('Happy New Year!', 'Happy Holidays!')");
    }
}
