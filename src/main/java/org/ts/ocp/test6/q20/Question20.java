package org.ts.ocp.test6.q20;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Question20 {
    public static void main(String[] args) throws InterruptedException {
        LocalDateTime start = LocalDateTime.now();
        //noinspection Convert2Lambda,RedundantThrows
        Callable<String> c = new Callable<String>() {
            @Override
            public String call() throws Exception {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException ignored) {
                }
                return "HELLO";
            }
        };

        ExecutorService es = Executors.newFixedThreadPool(10);
        List<Callable<String>> list = Arrays.asList(c, c, c, c, c);
        List<Future<String>> futures = es.invokeAll(list);
        System.out.println(futures.size());
        es.shutdown();
        System.out.format("execution duration was = %s%n", Duration.between(start, LocalDateTime.now()));
    }
}
