package org.ts.ocp.test6.q76;

import org.ts.ocp.test6.DerbySetup;

import java.sql.*;
import java.util.Collections;
import java.util.List;

@SuppressWarnings("unused")
public class Question76 {
    public static void main(String[] args) throws SQLException {
//        String url = "jdbc:mysql://localhost:3306/ocp";
        String url = DerbySetup.setupDerby(getDdlSqls(),getDmlSqls());
//        user and password is not used because we don't use mysql but derby
//        String user = "root";
//        String password = "password";
        String query = "Select ID, MESSAGE FROM LOG";
        try (Connection con = DriverManager.getConnection(url/*, user, password*/);
             Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE)
        ) {
            stmt.executeUpdate("INSERT INTO LOG VALUES(1001, 'Login Successful')");
            stmt.executeUpdate("INSERT INTO LOG VALUES(1002, 'Login Failure')");
            
            con.setAutoCommit(false);
            
            stmt.executeUpdate("INSERT INTO LOG VALUES(1003, 'Not Authorized')");
            
        }
    }
    
    private static List<String> getDdlSqls(){
        return Collections.singletonList("CREATE TABLE LOG(ID INTEGER, MESSAGE VARCHAR(1000), PRIMARY KEY (ID))");
    }
    private static List<String> getDmlSqls(){
        return Collections.emptyList();
    }
    
    private static class AnswerVerifier{
        public static void main(String[] args) {
            String url = "jdbc:derby:ocp";
            try(Connection connection = DriverManager.getConnection(url); Statement statement = connection.createStatement()){
                final ResultSet rs = statement.executeQuery("SELECT * FROM LOG");
                DerbySetup.printSelectedTableColumns(rs,1,2);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
