package org.ts.ocp.test6.q46;

import org.ts.ocp.test6.DerbySetup;

import java.sql.*;
import java.util.Collections;
import java.util.List;

public class Question46 {
    @SuppressWarnings("SqlNoDataSourceInspection")
    public static void main(String[] args) {
        String url = DerbySetup.setupDerby(getDdlSqls(), getDmlSqls());
        try {
            Connection con = DriverManager.getConnection(url);
            String query = "Select * FROM EMPLOYEE";
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                System.out.println("ID: " + rs.getInt("IDD"));
                System.out.println("First Name: " + rs.getString("FIRSTNAME"));
                System.out.println("Last Name: " + rs.getString("LASTNAME"));
                System.out.println("Salary: " + rs.getDouble("SALARY"));
            }
            rs.close();
            stmt.close();
            con.close();
        } catch (SQLException ex) {
            System.out.println("An Error Occurred!");
            ex.printStackTrace();
        }
    }

    private static List<String> getDdlSqls() {
        return Collections.singletonList(
                "CREATE TABLE EMPLOYEE (ID INTEGER, FIRSTNAME VARCHAR(100), LASTNAME VARCHAR(100), SALARY REAL, PRIMARY KEY (ID))");
    }

    private static List<String> getDmlSqls() {
        return Collections.singletonList("INSERT INTO EMPLOYEE VALUES (101, 'John', 'Smith', 12000)");
    }
}
