package org.ts.ocp.test6.q43;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

class Counter implements Runnable {
    private static AtomicInteger ai = new AtomicInteger(100);

    public void run() {
        System.out.println(ai.getAndDecrement());
    }
}

public class Question43 {
    public static void main(String[] args) {
        Thread[] threads = IntStream.range(0, 100)
                                    .mapToObj(i -> new Thread(new Counter()))
                                    .toArray(Thread[]::new);

        for (Thread thread : threads) {
            thread.start();
        }
    }
}
