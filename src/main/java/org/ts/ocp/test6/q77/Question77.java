package org.ts.ocp.test6.q77;

import org.ts.ocp.test6.DerbySetup;

import java.sql.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@SuppressWarnings({"SqlNoDataSourceInspection", "SqlResolve"})
public class Question77 {
    public static void main(String[] args) throws SQLException {
//        String url = "jdbc:mysql://localhost:3306/ocp";
        String url = DerbySetup.setupDerby(getDdlSqls(), getDmlSqls());
//        user and password is not used because we don't use mysql but derby
//        String user = "root";
//        String password = "password";
        String query = "Select ID, FIRSTNAME, LASTNAME, SALARY FROM EMPLOYEE ORDER BY ID";
        try (Connection con = DriverManager.getConnection(url/*, user, password*/);
             Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)
        ) {
            ResultSet rs = stmt.executeQuery(query);
            rs.relative(-3);
            rs.relative(1);
            System.out.println(rs.getInt(1));
        }
    }

    private static List<String> getDdlSqls() {
        return Collections.singletonList(
                "CREATE TABLE EMPLOYEE (ID INTEGER, FIRSTNAME VARCHAR(100), LASTNAME VARCHAR(100), SALARY REAL, PRIMARY KEY (ID))");
    }

    private static List<String> getDmlSqls() {
        return Arrays.asList("INSERT INTO EMPLOYEE VALUES(101,'John','Smith',12000)",
                             "INSERT INTO EMPLOYEE VALUES(102,'Sean','Smith', 15000)",
                             "INSERT INTO EMPLOYEE VALUES(103,'Regina','Williams',15500)",
                             "INSERT INTO EMPLOYEE VALUES(104,'Natasha','George',14600)");
    }
}
