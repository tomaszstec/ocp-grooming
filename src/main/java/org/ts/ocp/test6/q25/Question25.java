package org.ts.ocp.test6.q25;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.RecursiveTask;

public class Question25 {
    public static void main(String[] args) {
//        AnswerA a = new AnswerA();
//        ForkJoinPool pool = new ForkJoinPool(4);
//        pool.invoke(a);
    }
}
class AnswerA extends RecursiveTask<Long> {
    @Override
    protected Long compute() {
        return null;
    }
}
class AnswerB extends RecursiveTask{
    @Override
    protected Long compute() {
        return null;
    }
}
class AnswerC /*extends RecursiveAction*/{
//    @Override
//    protected Long compute() {    //compilation error because compute() in ... clashes with compute() in java.util.concurrent.RecursiveAction; attempting to use incompatible return type
//        return null;
//    }
}
class AnswerD extends RecursiveTask<Object>{
    @Override
    protected Long compute() {
        return null;
    }
}
class AnswerE /*extends RecursiveAction*/{
//    @Override
//    protected Long compute() {    //compilation error because compute() in ... clashes with compute() in java.util.concurrent.RecursiveAction; attempting to use incompatible return type
//        return null;
//    }
}
