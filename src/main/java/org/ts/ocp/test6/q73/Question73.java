package org.ts.ocp.test6.q73;

import org.ts.ocp.test6.DerbySetup;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Question73 {
    public static void main(String[] args) throws Exception {
//        String url = "jdbc:mysql://localhost:3306/ocp";     //originally the mysql DB was used in the question,
        String url = DerbySetup.setupDerby(getDdlSqls(), getDmlSqls());
//        user and password is not used because we don't use mysql but derby
//        String user = "root";
//        String password = "password";
        String query = "Select ID, FIRSTNAME, LASTNAME, SALARY FROM EMPLOYEE ORDER BY ID";
        try (Connection con = DriverManager.getConnection(url/*, user, password*/);
             Statement stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE)
        ) {
            ResultSet rs = stmt.executeQuery(query);
            rs.moveToInsertRow();   // derby does not support ResultSet.CONCUR_UPDATABLE statements
            rs.updateInt(1, 105);
            rs.updateString(2, "Chris");
            rs.updateString(3, "Lee");
            rs.updateDouble(4, 16000);
            rs.refreshRow(); //Line n1
            rs.insertRow(); //Line n2
            rs.last();
            System.out.println(rs.getInt(1)); //Line n3
        }
    }
    private static List<String> getDdlSqls() {
        return Collections.singletonList(
                "CREATE TABLE EMPLOYEE (ID INTEGER, FIRSTNAME VARCHAR(100), LASTNAME VARCHAR(100), SALARY REAL, PRIMARY KEY (ID))");
    }
    
    private static List<String> getDmlSqls() {
        return Arrays.asList("INSERT INTO EMPLOYEE VALUES(101,'John','Smith',12000)",
                             "INSERT INTO EMPLOYEE VALUES(102,'Sean','Smith', 15000)",
                             "INSERT INTO EMPLOYEE VALUES(103,'Regina','Williams',15500)",
                             "INSERT INTO EMPLOYEE VALUES(104,'Natasha','George',14600)");
    }
    
}
