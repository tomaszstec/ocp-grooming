package org.ts.ocp.test6.q24;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

public class Question24 {
    public static void main(String[] args) {
        char answerLetter = 'A';
        printAnswer(answerLetter++, ai -> ai.addAndGet(1), String::valueOf);
        printAnswer(answerLetter++, ai -> ai.getAndAdd(1), ai -> String.valueOf(ai.get()));
        printAnswer(answerLetter++, AtomicInteger::getAndIncrement, ai -> String.valueOf(ai.get()));
        printAnswer(answerLetter, AtomicInteger::incrementAndGet, ai -> String.valueOf(ai.get()));
//        printAnswer(answerLetter++, ai->ai.incrementAndGet(1), ai->String.valueOf(ai));   // compilation error
    }

    private static void printAnswer(char answerLetter, Function<AtomicInteger, Integer> param1, Function<AtomicInteger, String> param2) {
        AtomicInteger ai = new AtomicInteger(10);
        System.out.format("%c = %s:%s%n", answerLetter, param1.apply(ai), param2.apply(ai));
    }
}
